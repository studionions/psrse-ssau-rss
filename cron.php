<?php
header('Content-type: text/plain; charset=utf-8');
/*
 *
 * в .htaccess нужно указать запрет на прямой доступ к данному файлу
 *
 <Files cron.php>
	Order Deny,Allow
	Deny from all
 </Files>
 *
*/
$re = '/<\/h1>(?:\s+)?(<div.+)<br>(?:\s+)?<div class="tag">/Usi';
$reIndex = '/(\d+)/';
$assetsPath = "assets/images/ssau/";
$imagePath = str_replace("\\", "/", dirname(__FILE__))."/".$assetsPath;
$xml = "http://ssau.ru/rss_abitur/";

if(!is_dir($imagePath)):
	@mkdir($imagePath, 0777, true);
endif;

$rss = @simplexml_load_file($xml);

if($rss):
	$items = array();
	$offset = 86400 * 1;// Пока три дня, но будет один день. Нужно внести все пропущенные новости при первом запуске скрипта.
	$parent = 208; // ID раздела, куда складываем новости.
	$curentDate = strtotime(date("d-m-Y", time()-$offset));
	$results = array();
	foreach ($rss->channel->item as $item):
		/* 
		 * Все ключи массива результата соответствуют конкретной настройки сайта на Evolution CMS
		 *
		 * 
		 */
		$stamp = strtotime($item->pubDate);
		if($stamp < $curentDate)
			break;
		$link = trim((string)$item->link);
		preg_match($reIndex, $link, $matches);
		$idssau = $matches[1];
		$img  = trim((string)$item->enclosure->attributes()->url);
		$extension = pathinfo($img, PATHINFO_EXTENSION);
		$fname_img = "{$idssau}.{$extension}";
		
		/*
		 * template -> ID шаблона новости
		 * published -> Сразу публикуем
		 * parent -> ID раздела родителя, куда складываем новости.
		 * hidemenu -> Прячем меню
		 * pagetitle -> Заголовок 
		 * longtitle -> расширенный заголовок
		 * introtext -> Вступительный текст
		 * description -> Описание
		 * createdon -> Время (дата) создания страницы
		 * publishedon -> Время (дата) публикации страницы
		 * createdby -> ID Менеджера, от имени которого происходит публикация
		 * publishedby -> ID Менеджера, от имени которого происходит публикация
		 * editedby -> ID Менеджера, от имени которого происходит публикация
		 * imageSoc -> TV параметр. В нашем случае это картинка, которая отображается как перевью новости и для отображения в соц. сетях при шаринге ссылки.
		 * news_date -> TV параметр даты публикаций.
		 * siteUrl -> ID подразделения. (На нашем сайте существует три подразделения)
		 * news_person -> TV параметр Автор новости.
		 * content -> Непосредственно основной контент
		 */
		$title = trim((string)$item->title);
		$description = trim((string)$item->description);
		
		// Массив значений ресурса для его создания в Evolution CMS
		$result = array(
			'template'        => 12,
			'published'       => 1,
			'parent'          => $parent,
			'hidemenu'        => 1,
			'longtitle'       => $title,
			'pagetitle'       => $title,
			'introtext'       => $description,
			'description'     => $description,
			'createdon'       => $stamp,
			'publishedon'     => $stamp,
			'createdby'       => 1,
			'editedby'        => 1,
			'publishedby'     => 1,
			'imageSoc'        => "{$assetsPath}{$fname_img}",
			'news_date'       => date("d-m-Y H:m:s",strtotime($item->pubDate)),
			'siteUrl'         => "1",
			'news_person'     => "Самарский университет",
			'content'         => ''
		);
		
		// Сохраняем изображение
		$rimg = @file_put_contents($imagePath.$fname_img, @file_get_contents($img));
		// Получаем страницу новости и обрабатываем.
		$content = mb_convert_encoding(file_get_contents($link), "utf-8", "windows-1251");
		preg_match($re, $content, $contents);
		$content = trim($contents[1]);
		$result['content'] = preg_replace(
			// Приводим таблицы к CSS правилам фреймфорка Bootstrap
			'/<table (.+)>/Ui',
			'<table class="table table-bordered">',
			preg_replace(
				// Удаляем лишние пробелы, переносы строк, табы
				'/\s{2,}/',
				'',
				preg_replace(
					// Открываем все ссылки в новой вкладке/окне
					'/<a (.+)>/Usi',
					'<a $1 target="_blank" rel="nofollow">',
					preg_replace(
						// Удаляем пустые абзацы.
						'/<p>\s+<\/p>/i',
						'',
						// Приводим вёрстку контента в нормальный SEO вид
						// Абзацы основного контента должны быть абзацами!!!, а не тупыми блоками div
						str_ireplace('div', 'p', $content)
					)
				)
			)
		)."<p>&nbsp;</p><p class=\"text-right\"><strong>Источник:</strong> <a href=\"{$link}\" target=\"_blank\" rel=\"nofollow\">ssau.ru</a></p>";
		// Данные готовы для создания документа.
		$results[] = $result;
	endforeach;
	
	/*
	 * API Evolution CMS
	*/
	if($results):
		// Подключаем API Evolution CMS
		define('MODX_API_MODE', true);
		include_once(dirname(__FILE__)."/index.php");
		$modx->db->connect();
		if (empty($modx->config)) {
			$modx->getSettings();
		}
		if(!defined('MODX_BASE_PATH')):
			define('MODX_BASE_PATH', dirname(__FILE__)."/");
		endif;
		
		// Подключаем класс для создания, редактирования, удаления ресурсов
		include_once(MODX_BASE_PATH."assets/lib/resourse.php");
		// полное имя таблицы контента сайта.
		$table = $modx->getFullTableName("site_content");
		// Реверс массива.
		$resultss = array_reverse($results);
		foreach($resultss as $key=>$value):
			// Создаём ресурс
			$resourse = resourse::Instance(0);
			foreach($value as $k=>$v):
				// Экранируем
				$v = $modx->db->escape($v);
				// Устанавливаем параметры
				$resourse->set($k, $v);
			endforeach;
			// Получаем максимальный menuindex для документов родителя
			$sql = "SELECT MAX(`menuindex`) FROM ".$table . " WHERE parent='{$parent}'";
			$res = $modx->db->query($sql);
			$id = $modx->db->getValue($res);
			// Увеличиваем на 1
			$id = (((string)$id)."" == "") ? 0 : $id+1;
			// Устанавливаем menuindex
			$resourse->set('menuindex', $id);
			// Сохраняем ресурс
			$id = $resourse->save(1);
			// Чистим кешь
			$resourse->clear_chache();
			// Удаляем из памяти.
			unset($resourse);
			/*
			* minobr63
			* Дадим мускулу успокоиться
			*/
			sleep(10);
		endforeach;
	endif;
endif;